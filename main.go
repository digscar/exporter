package main

import (
	"context"
	"log"

	viper "github.com/spf13/viper"
)

func main() {

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./configs")
	viper.AddConfigPath("/etc/exporter/")
	viper.AddConfigPath("$HOME/.exporter")
	viper.AddConfigPath(".")
	viper.SetEnvPrefix("EXPORTER")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("Fatal error config file: %s \n", err)
	}

	StreamMessages(context.Background())
}
