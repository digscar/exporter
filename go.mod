module github.tattsgroup.com/TattsGroup/exporter

go 1.13

require (
	github.com/golang/protobuf v1.3.3
	github.com/spf13/viper v1.6.2
	github.com/tidwall/gjson v1.6.0
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
	google.golang.org/grpc v1.28.0
)
